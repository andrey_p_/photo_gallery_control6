class CreatePhotoComments < ActiveRecord::Migration
  def change
    create_table :photo_comments do |t|
      t.text :description
      t.integer :rating

      t.timestamps null: false
    end
  end
end
